package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * SkuFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "skuFeign")
@RequestMapping("/sku")
public interface SkuFeign {
    @PostMapping(value = "/decrease")
    Result<String> decrease(@RequestBody Map<Long, Integer> map);

}