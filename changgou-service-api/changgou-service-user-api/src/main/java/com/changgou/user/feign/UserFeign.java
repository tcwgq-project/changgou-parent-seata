package com.changgou.user.feign;

import com.changgou.common.response.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * UserFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "userFeign")
@RequestMapping("/user")
public interface UserFeign {
    @PostMapping("/points/add")
    Result<String> addPoints(@RequestParam(value = "points") Integer points,
                             @RequestParam(value = "username") String username);

}