package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * OrderFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "orderFeign")
@RequestMapping("/order")
public interface OrderFeign {
    @PostMapping("/create")
    Result<Order> create(@RequestBody Order order);

}