package com.changgou.user.service;

/**
 * UserService
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface UserService {
    int addPoints(Integer points, String username);

}
