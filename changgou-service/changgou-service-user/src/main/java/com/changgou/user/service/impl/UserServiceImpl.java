package com.changgou.user.service.impl;

import com.changgou.user.dao.UserMapper;
import com.changgou.user.service.UserService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * UserServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @GlobalTransactional
    @Override
    public int addPoints(Integer points, String username) {
        log.info("User Seata全局事务id=================>{}", RootContext.getXID());
        // 使用数据库保证原子操作
        int row = userMapper.addPoints(points, username);
        log.info("用户增加积分结果 row={}", row);
        // int a = 1 / 0;
        return row;
    }

}
