package com.changgou.user.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.user.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * UserController
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/points/add")
    public Result<String> addPoints(@RequestParam(value = "points") Integer points,
                                    @RequestParam(value = "username") String username) {
        userService.addPoints(points, username);
        return new Result<>(true, StatusCode.OK, "添加积分成功");
    }

}
