package com.changgou.user.dao;

import com.changgou.user.bean.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/**
 * UserDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface UserMapper extends Mapper<User> {
    @Update(value = "update tb_user set points = points + #{points} where username = #{username}")
    int addPoints(@Param(value = "points") Integer points, @Param(value = "username") String username);

}
