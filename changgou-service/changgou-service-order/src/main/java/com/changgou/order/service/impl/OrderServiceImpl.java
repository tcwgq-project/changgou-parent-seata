package com.changgou.order.service.impl;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.response.Result;
import com.changgou.common.utils.IdWorker;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.order.bean.Order;
import com.changgou.order.dao.OrderMapper;
import com.changgou.order.service.OrderService;
import com.changgou.user.feign.UserFeign;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * OrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private IdWorker idWorker;

    @Resource
    private SkuFeign skuFeign;

    @Resource
    private UserFeign userFeign;

    @Resource
    private OrderMapper orderMapper;

    @GlobalTransactional
    @Override
    public Order create(Order order) {
        log.info("Order Seata全局事务id=================>{}", RootContext.getXID());
        // 获取订单明细
        order.setUsername(order.getUsername());
        order.setId(idWorker.nextId() + "");
        order.setTotalNum(1);
        order.setTotalMoney(1);
        order.setPreMoney(1);
        order.setPayMoney(1);
        order.setPayType("1");
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        order.setSourceType("1");
        order.setTransactionId("?");
        order.setOrderStatus("0");
        order.setPayStatus("0");
        order.setIsDelete("0");
        orderMapper.insertSelective(order);

        List<Long> skuList = order.getSkuList();
        // 减库存
        Map<Long, Integer> map = skuList.stream().collect(Collectors.toMap(Function.identity(), num -> 1));
        Result<String> decrease = skuFeign.decrease(map);
        if (!decrease.getCode().equals(StatusCode.OK)) {
            throw new ChanggouException(decrease.getCode(), decrease.getMessage());
        }

        // 添加积分
        Result<String> addPoints = userFeign.addPoints(1, order.getUsername());
        if (!addPoints.getCode().equals(StatusCode.OK)) {
            throw new ChanggouException(addPoints.getCode(), addPoints.getMessage());
        }

        return order;
    }

}
