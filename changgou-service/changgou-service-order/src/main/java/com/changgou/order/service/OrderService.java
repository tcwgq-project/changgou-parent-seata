package com.changgou.order.service;

import com.changgou.order.bean.Order;

/**
 * OrderService
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderService {
    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    Order create(Order order);

}
