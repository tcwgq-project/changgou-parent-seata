package com.changgou.order.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.order.bean.Order;
import com.changgou.order.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * OrderController
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {
    @Resource
    private OrderService orderService;

    @PostMapping("/create")
    public Result<Order> create(@RequestBody Order order) {
        Order result = orderService.create(order);
        return new Result<>(true, StatusCode.OK, "下单成功", result);
    }

}
