package com.changgou.goods.config;

import com.changgou.common.utils.IdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tcwgq
 * @since 2022/5/28 16:35
 */
@Configuration
public class IdConfig {
    @Bean
    public IdWorker idWorker() {
        return new IdWorker(0, 0);
    }

}
