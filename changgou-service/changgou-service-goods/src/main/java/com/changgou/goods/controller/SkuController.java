package com.changgou.goods.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.goods.service.SkuService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Api(value = "SkuController")
@RestController
@RequestMapping("/sku")
@CrossOrigin
public class SkuController {
    @Resource
    private SkuService skuService;

    @PostMapping(value = "/decrease")
    public Result<String> decrease(@RequestBody Map<Long, Integer> map) {
        skuService.decrease(map);
        return new Result<>(true, StatusCode.OK, "减少库存成功");
    }

}
