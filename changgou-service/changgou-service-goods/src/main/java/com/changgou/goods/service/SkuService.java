package com.changgou.goods.service;

import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface SkuService {
    void decrease(Map<Long, Integer> map);

}
