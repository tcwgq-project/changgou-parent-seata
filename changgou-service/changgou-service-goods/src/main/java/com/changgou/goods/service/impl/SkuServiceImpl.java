package com.changgou.goods.service.impl;

import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.goods.dao.SkuMapper;
import com.changgou.goods.service.SkuService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Slf4j
@Service
public class SkuServiceImpl implements SkuService {
    @Resource
    private SkuMapper skuMapper;

    @GlobalTransactional
    @Override
    public void decrease(Map<Long, Integer> map) {
        log.info("Sku Seata全局事务id=================>{}", RootContext.getXID());
        /*
         * 先查询再更新在这里不能使用，并发环境下不能保证原子性
         * TODO 高并发的扣库存如何实现？
         */
        map.forEach((skuId, num) -> {
            int i = skuMapper.decrease(skuId, num);
            if (i <= 0) {
                throw new ChanggouException(ChanggouErrorCode.DECREASE_GOOD_NUM_FAIL);
            }
        });
    }

}
