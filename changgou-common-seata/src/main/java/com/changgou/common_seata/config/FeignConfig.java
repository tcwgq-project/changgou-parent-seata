package com.changgou.common_seata.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import io.seata.core.context.RootContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author tcwgq
 * @since 2022/6/21 23:26
 */
@Configuration
public class FeignConfig {
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new AdminTokenInterceptor();
    }

    private static class AdminTokenInterceptor implements RequestInterceptor {
        @Override
        public void apply(RequestTemplate template) {
            // 将原始请求头信息原样传到调用的微服务
            /*
             * TODO
             * 开启feign熔断，默认是线程池隔离时，requestAttributes为null，需要配置为信号量隔离
             */
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                // 1.获取请求对象
                HttpServletRequest request = requestAttributes.getRequest();
                Enumeration<String> headerNames = request.getHeaderNames();
                if (headerNames != null) {
                    // 2.获取请求对象中的所有的头信息(网关传递过来的)
                    while (headerNames.hasMoreElements()) {
                        String name = headerNames.nextElement();// 头的名称
                        String value = request.getHeader(name);// 头名称对应的值
                        // 3.将头信息传递给feign (restTemplate)
                        template.header(name, value);
                    }
                }
                String xid = request.getHeader(RootContext.KEY_XID.toLowerCase());
                boolean isBind = false;
                if (StringUtils.isNotBlank(xid)) {
                    RootContext.bind(xid);
                    isBind = true;
                }
                try {
                    template.header(RootContext.KEY_XID.toLowerCase(), xid);
                } finally {
                    if (isBind) {
                        RootContext.unbind();
                    }
                }
            }
        }

    }

}
